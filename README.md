# Investment website exercise

## Introduction

Let's assume our client is an investment company willing to empower their 
users. The company has access list of trading orders (in JSON format), 
representing every asset bought or sold from each of their users.

The investment company now wants to give their users a way to view their 
capital gain/loss.

To display capital gain/loss, we have to calculate the **cost basis** for a 
given asset, using a FIFO (First In, First Out) queue. In short, every time 
a user sells an asset, we consider that the asset sold was the first (older) 
to have been bought.

More information about cost basis can be found here: 
https://www.investopedia.com/ask/answers/05/costbasis.asp

## Example

For example, the following JSON:
```json
[{
  "product": "GME",
  "date": "01/01/2021",
  "size": 1,
  "price": 5,
  "fees": 0.5
} , {
  "product": "GME",
  "date": "01/02/2021",
  "size": 2,
  "price": 10,
  "fees": 2
} , {
  "product": "GME",
  "date": "01/03/2021",
  "size": -1.5,
  "price": 20,
  "fees": 3
}]
```

Would translate into:
* 01/01/2021: Bought 1 GME at $5/share with fees of $0.50
* 01/02/2021: Bought 2 GME at $10/share with fees of $2
* 01/03/2021: Sold 1.5 GME at $20/share with fees of $3

And the cost basis would be:

| Sell date	| Buy date | Amount	| Price	| Fees	| Sell Total | Buy Total | Gain/Loss |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 01/03/2021 | 01/02/2021 |	1.5	| 20 | 3 | 27 | 11 | 16 (+145.45%) |


### Total sold

The total sold can be calculated directly from the JSON file: 
```
1.5 * 20 - 3 = 27.
```

### Total bought

For the total bought, we are using a FIFO queue (first item bought is the 
first to be sold):
* 1 share bought at $5/share on 01/01/2021:
```
1 * 5 + (1 / 1) * 0.5 = 5.5
```
* 0.5 share bought at $10/share on 01/02/2021:
```
0.5 * 10 + (0.5 / 2) * 2 = 5.5
```

The total bought is then:
```
5.5 + 5.5 = 11
```

### Gain / Loss

The gain / loss is calculated by substracting the sell total from the buy 
total. 
```
27 - 11 = 16
```
Which represents a gain of `16 / 11 * 100 = 145.45%`.

## Exercise

The exercise is to create:
1. A backend able to store the information contained in the JSON files.
2. A frontend giving the user the ability to:
    1. Upload the JSON formatted files. 
    2. View the cost basis in a list similar to the screenshot below.

!["Sample Cost Basis"](./cost_basis_sample.png "Sample Cost Basis")

For the exercise, we will limit ourselves to a single product and assume the 
data only belongs to one client.

## Data Generation

Some orders are available in the data folder but feel free to generate more
using the *generate.py* script. 

Help is available by typing `python generate.py --help`.
