""" Order list generator.
"""
import argparse
import random
import json
from datetime import datetime, timedelta
from os.path import dirname, realpath, join

DATA_FOLDER = "%s/data" % dirname(realpath(__file__))


def generate_order(order_date):
    """Generate a random order.

    Returns:
         - dict: The order with product, date, size, price and fees keys.
    """
    genrated_order = {
        "product": "GME",
        "date": order_date.strftime("%Y-%m-%d"),
        "size": round(random.random() + random.randint(1, 19), 5),
        "price": round(random.random() + random.randint(5, 9), 2),
    }

    # Fees are 5% of the order.
    genrated_order["fees"] = round(
        genrated_order["price"] * genrated_order["size"] * 0.05, 2
    )

    return genrated_order


if __name__ == "__main__":
    # Setup the argument parser
    parser = argparse.ArgumentParser(
        description="Tool to generate JSON file containing trading orders."
    )
    parser.add_argument("--filename", type=str, help="name of the output file")
    parser.add_argument("--count", type=int, help="number of orders to generate")
    parser.add_argument("--seed", type=str, help="random seed for data generator")

    args = parser.parse_args()

    # Input variables
    seed = args.seed if args.seed else random.getrandbits(32)
    order_count = args.count if args.count else 15
    output_file = args.filename if args.filename else "orders.%s.json" % seed

    print("Generating %d orders in %s (seed=%s)..." % (order_count, output_file, seed))

    # Setup program variables
    random.seed(seed)
    available_assets = 0
    order_list = list()
    date = datetime.today()

    while order_count > len(order_list):
        # Random number of buy event to make sure user has assets.
        start_buy_order_count = min(random.randint(3, 5), order_count - 1)

        for _ in range(start_buy_order_count):
            date += timedelta(days=random.randint(0, 2))
            buy_order = generate_order(date)
            available_assets += buy_order["size"]
            order_list.append(buy_order)

        # Ensure that there is at least one sale order.
        is_sale_order = True

        while available_assets > 0:
            date += timedelta(days=random.randint(0, 2))
            order = generate_order(date)

            if is_sale_order:
                order["size"] = -min(order["size"], available_assets)

            available_assets += order["size"]
            order_list.append(order)

            # Randomly decide if the next order is a sell or buy order
            is_sale_order = random.randint(1, 10) > 5

            if order_count == len(order_list):
                break

    # Dump json into the requested file
    with open(join(DATA_FOLDER, output_file), "w") as output_fp:
        json.dump(order_list, output_fp, indent=2)
